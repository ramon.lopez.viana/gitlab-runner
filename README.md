# Gitlab Runner

Para probar instalación de los runners.

# Instalación del runner en OSX

[Referencia](https://docs.gitlab.com/runner/install/osx.html)

# Requistos

[Referencia](https://docs.gitlab.com/runner/configuration/macos_setup.html)

Change the system shell to Bash.
Change the system shell to Bash
Newer versions of macOS ship with Zsh as the default shell. You must change it to Bash.

Connect to your machine and determine the default shell:

echo $shell

If the result is not /bin/bash, change the shell by running:

chsh -s /bin/bash

Enter your password.
Restart your terminal or reconnect by using SSH.
Run echo $SHELL again. The result should be /bin/bash.


Install Homebrew, rbenv, and GitLab Runner.

Install the Homebrew package manager:

/bin/bash -c "$(curl "https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh")"

Set up rbenv, which is a Ruby version manager, and GitLab Runner:

brew install rbenv gitlab-runner
brew services start gitlab-runner

Configure rbenv and install Ruby.

Configure rbenv and install Ruby
Now configure rbenv and install Ruby.

Add rbenv to the Bash environment:

echo 'if which rbenv > /dev/null; then eval "$(rbenv init -)"; fi' >> ~/.bash_profile
source ~/.bash_profile

Install Ruby 2.74 and set it as the machine’s global default:

rbenv install 2.7.4
rbenv global 2.7.4

**Check Ruby Environment**

ruby rubyM1.rb


Install Xcode
Now install and configure Xcode.

Go to one of these locations and install Xcode:

The Apple App Store.
The Apple Developer Portal.
xcode-install. This project aims to make it easier to download various Apple dependencies from the command line.
Agree to the license and install the recommended additional components. You can do this by opening Xcode and following the prompts, or by running the following command in the terminal:

sudo xcodebuild -runFirstLaunch

Update the active developer directory so that Xcode loads the proper command line tools during your build:

sudo xcode-select -s /Applications/Xcode.app/Contents/Developer

Register a runner.
Configure CI/CD.

# Instalando Android Studio SDK

brew tap homebrew/cask

**Instalar las Command Line Tools**

brew install --cask android-commandlinetools

Para la instalación con cask es necesario disponer del rb correspondiente (se extrae de github).

Se pueden recuperar de aquí
https://developer.android.com/studio#command-tools


unzip


$ cd cmdline-tools
$ mkdir tools
$ mv -i * tools

export ANDROID_HOME=$HOME/android
export PATH=$ANDROID_HOME/cmdline-tools/tools/bin/:$PATH
export PATH=$ANDROID_HOME/emulator/:$PATH
export PATH=$ANDROID_HOME/platform-tools/:$PATH

Instalar las tools

sdkmanager --install "platform-tools"

sdkmanager --install "platforms;android-29" --licenses
sdkmanager --install "build-tools;29.0.2" --licenses
sdkmanager --install "emulator" --licenses


## Instalación flutter

https://docs.flutter.dev/development/tools/sdk/releases?tab=macos


https://storage.googleapis.com/flutter_infra_release/releases/stable/macos/flutter_macos_2.10.5-stable.zip


Instalación de dart


## Obtención de binarios de instalación

Dos sabores (brew y binaria)

**Brew**
brew install gitlab-runner

**Tags disponibles**
https://gitlab.com/gitlab-org/gitlab-runner/-/tags

*macOS amd64*
sudo curl --output /usr/local/bin/gitlab-runner "https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-darwin-amd64"

*macOs intel*
# macOS Apple Silicon/arm64
sudo curl --output /usr/local/bin/gitlab-runner "https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-darwin-arm64"

## Instalación
sudo chmod +x /usr/local/bin/gitlab-runner

## Configuración

Los runners deben correr en modo shell dentro de los macOS. Hay que tenerlo en cuenta en la configuración.

su - <username>

Install GitLab Runner as a service and start it:

cd ~
gitlab-runner install
gitlab-runner start


El fichero de configuración (config.toml) estará disponible en /Users/gitlab-runner/.gitlab-runner/. 

Para configurarlo: https://docs.gitlab.com/runner/configuration/advanced-configuration.html


**Actualización**

gitlab-runner stop

Download the binary to replace the GitLab Runner executable:

sudo curl -o /usr/local/bin/gitlab-runner "https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-darwin-amd64"

You can download a binary for every available version as described in Bleeding Edge - download any other tagged release.

Give it permissions to execute:

sudo chmod +x /usr/local/bin/gitlab-runner

Start the service:

gitlab-runner start




## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/ramon.lopez.viana/gitlab-runner.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/ramon.lopez.viana/gitlab-runner/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!).  Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
